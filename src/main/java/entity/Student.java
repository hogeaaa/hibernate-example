package entity;

import javax.persistence.*;
import java.util.Objects;
@NamedQueries({
        @NamedQuery(name="find_student_by_name",query = "select s from Student s where s.firstName = :firstName"
        ),
        @NamedQuery(name = "insert_student",query = "INSERT INTO Student as s VALUES (s.firstName = :firstName,s.lastName = :lastName,s.email = :email,s.age = :age)")
})
@Entity
@Table(name = "Student")
public class Student {

    private static final String STUDENT_SEQUENCE = "student_id_sequence";
    private static final String STUDENT_GENERATOR = "student_generator";

    @Id
    @SequenceGenerator(name="STUDENT_GENERATOR", sequenceName = STUDENT_SEQUENCE)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = STUDENT_GENERATOR)
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "age")
    private int age;

    public Student() {
    }

    public static String getStudentSequence() {
        return STUDENT_SEQUENCE;
    }

    public static String getStudentGenerator() {
        return STUDENT_GENERATOR;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id &&
                age == student.age &&
                Objects.equals(firstName, student.firstName) &&
                Objects.equals(lastName, student.lastName) &&
                Objects.equals(email, student.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, email, age);
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                '}';
    }
}
