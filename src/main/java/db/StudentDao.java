package db;

import entity.Student;

import javax.persistence.Query;

public class StudentDao extends DbInitializer {
    public void insertStudent(Student student){
        openSessionAndTransaction();
        session.persist(student);
        closeSessionAndTransaction();
    }

    public Student findStudentById(int id){
        openSessionAndTransaction();
        Student student = session.find(Student.class,id);
        closeSessionAndTransaction();
        return student;
    }

    public void updateStudent(Student student){
        openSessionAndTransaction();
        session.update(student);
        closeSessionAndTransaction();
    }

    public void deleteStudent(int id){
        openSessionAndTransaction();
        Student student = session.find(Student.class,id);
        session.delete(student);
        closeSessionAndTransaction();
    }
    public Student findStudentByFirstName(String firstName){
        openSessionAndTransaction();
        Query query = session.createNamedQuery("find_student_by_name");
        query.setParameter("firstName", firstName);
        Student student = (Student) query.getSingleResult();
        closeSessionAndTransaction();
        return student;
    }
    public void insertStudent(Student student){
        openSessionAndTransaction();
        Query query = session.createNamedQuery("insert_student");
        query.setParameter("firstName",student.setFirstName());
    }
}
