package db;

import entity.Student;

public class Main {
    public static void main(String[] args) {
        Student student = new Student();
        student.setFirstName("Andrei");
        student.setLastName("Hogea");
        student.setEmail("andrei@yahoo.com");
        student.setAge(22);

        StudentDao studentDao = new StudentDao();
        studentDao.insertStudent(student);
//        System.out.println(studentDao.findStudentById(1));
//        student.setId(1);
//        student.setFirstName("Boreja");
//        studentDao.updateStudent(student);
           studentDao.deleteStudent(52);
           studentDao.deleteStudent(102);
        System.out.println(studentDao.findStudentByFirstName("Andrei"));
    }
}
